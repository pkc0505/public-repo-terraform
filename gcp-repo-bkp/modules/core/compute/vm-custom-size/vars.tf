variable "region" {
  description   = "The zone name the server will use for VM requests"
  type          = string
  #default       = "asia-south1"
}
variable "app_name" {
  description   = "The name the server will use for app_name requests"
  type          = string
}
#environment only variable allowed is dav, uat, stage, prod
variable "environment" {
  description   = "The name the server will use for VM requests"
  type          = string
}
variable "vm_count" {
  description   = "No of VM requests"
  type          = number
}
variable "vm_zone" {
  description   = "The zone name the server will use for VM requests"
  type          = string
}
variable "machine_type" {
  description   = "The zone name the server will use for VM requests"
  type          = string
  default       = "f1-micro"
}
variable "vm_image" {
  description   = "The zone name the server will use for VM requests"
  type          = string
  default       = "debian-cloud/debian-9"
}
variable "fw_allow_protocol" {
  description   = "The zone name the server will use for VM requests"
  type          = string
  default       = "tcp"
}
variable "fw_allow_ports" {
  description   = "The zone name the server will use for VM requests"
  type          = list
  #default       = ["80", "22", "2222"]
}
variable "subnet_ip_cidr_range" {
  description   = "The zone name the server will use for VM requests"
  type          = string
  #default       = "10.158.0.0/20"
}
