resource "random_id" "instance_id" {
    byte_length     = 8
}

data "google_compute_zones" "available" {}

resource "google_compute_instance" "custom-size" {
    count           = var.vm_count
    name            = "vm-${var.app_name}-${count.index}-${var.environment}"
    machine_type    = var.machine_type
    zone            = var.vm_zone

    tags = ["tf", "${var.environment}", "${var.app_name}", "vm-${var.app_name}-${var.environment}", 
    "vm-${var.app_name}-${count.index}-${var.environment}"]

    boot_disk {
        initialize_params {
            image = "${var.vm_image}"
        }
    }

    network_interface {
        network     = "${data.google_compute_network.vpc.id}"
        subnetwork  = google_compute_subnetwork.subnet-app-specific.name
        access_config {
            // Ephemeral public IP
        }
    }

    metadata = {
        environment = "${var.environment}"
        app_name    = "${var.app_name}"
    }
}

resource "google_compute_firewall" "rules-app-specific" {
    name        = "firewall-rule-${var.app_name}-${var.environment}"
    network     = "${data.google_compute_network.vpc.id}"
    description = "Creates firewall rule targeting tagged instances"

    allow {
        protocol  = var.fw_allow_protocol
        ports     = var.fw_allow_ports #["80", "22", "8080", "1000-2000"]
    }

    target_tags = ["vm-${var.app_name}-${var.environment}"]
    source_tags = ["web", "vm-${var.app_name}-${var.environment}"]
}

resource "google_compute_subnetwork" "subnet-app-specific" {
    name          = "subnet-${var.app_name}"    
    ip_cidr_range = var.subnet_ip_cidr_range    
    region        = var.region
    network       = "${data.google_compute_network.vpc.id}"
    #region        = "asia-south1"
}

data "google_compute_network" "vpc" {
    name        = "vpc-${var.environment}"
}


#projects/test-netritvasadhana-org-2021/global/networks/vpc-stag