resource "google_storage_bucket" "gcs-bucket" {
    name          = "${var.name}"
    location      = "${var.location}"
    force_destroy = true
}

variable "location" {
  type      = string
  default   = "ASIA-SOUTH1"
}
variable "name" {
  type      = string
}