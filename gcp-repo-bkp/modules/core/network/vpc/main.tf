resource "google_compute_network" "vpc" {
    name                        = "vpc-${var.environment}"
    auto_create_subnetworks     = false
    description                 = "This VPC is for ${var.environment} enviornment"
    mtu                         = 1460
    routing_mode                = "${var.routing_mode}"
}

variable "environment" {
  type          = string
}

variable "routing_mode" {
  description   = "The zone name the server will use for VM requests"
  type          = string
  default       = "GLOBAL"
}
