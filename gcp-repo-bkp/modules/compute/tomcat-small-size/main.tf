module "tomcat-small-size" {
    source      = "../../core/compute/vm-custom-size"
    region      = "${var.region}"
    vm_zone     = "${var.zone}"
    app_name    = "${var.app_name}"
    environment = "${var.environment}"
    vm_count    = "${var.vm_count}"

    machine_type    = "f1-micro"
    vm_image        = "debian-cloud/debian-9"

    fw_allow_protocol  = "tcp"
    fw_allow_ports  = "${var.fw_allow_ports}"
    
    subnet_ip_cidr_range    = "${var.subnet_ip_cidr_range}"
}