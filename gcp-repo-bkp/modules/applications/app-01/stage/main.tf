terraform {
  backend "gcs" {
      prefix = "terraform-states/applications/app-01/stage"
  }
}
provider "google" {
    project         = var.project
    region          = var.region
    credentials     = var.credentials
}

locals {
    region          = "asia-south1"
    zone            = "asia-south1-a"
    environment     = "stage"
    app_name        = "main-website-ns-org"
    vm_count        = 2
}

module "vm-test-appname" {
    source      = "../../../modules/compute/tomcat-small-size"
    region      = local.region
    zone        = local.zone
    environment = local.environment
    app_name    = local.app_name
    
    vm_count    = local.vm_count

    fw_allow_ports  = ["80", "8080", "22", "10000-10010"]
    
    subnet_ip_cidr_range    = "10.158.0.0/20"
}