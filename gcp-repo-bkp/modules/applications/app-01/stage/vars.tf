variable "gcs_bucket" {
  description   = "The zone name the server will use for VM requests"
  type          = string
  #default       = "asia-south1"
}
variable "gcp_prefix" {
  description   = "The zone name the server will use for VM requests"
  type          = string
  #default       = "asia-south1"
}