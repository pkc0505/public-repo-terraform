locals {
    region          = "asia-south1"
    zone            = "asia-south1-a"
    environment     = "stage"
    app_name        = "test-appname"    
}

module "vm-test-appname" {
    source      = "../../../modules/compute/tomcat-small-size"
    region      = local.region
    zone        = local.zone
    environment = local.environment
    app_name    = local.app_name
    
    vm_count    = 2

    fw_allow_ports  = ["80", "8080", "22", "10000-10010"]
    
    subnet_ip_cidr_range    = "10.158.0.0/20"
}