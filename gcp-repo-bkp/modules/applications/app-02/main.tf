terraform {
  backend "gcs" {
      #configure backend
  }
}
provider "google" {}

module "vm-test-appname" {
    source      = "../../modules/compute/tomcat-small-size"
    region      = var.region
    zone        = var.zone
    environment = var.environment
    app_name    = var.app_name
    
    vm_count    = var.vm_count

    fw_allow_ports  = ["80", "8080", "22", "10000-10010"]
    
    subnet_ip_cidr_range    = var.subnet_ip_cidr_range
}

variable "region" {
  type          = string
  default       = "asia-south1"
}
variable "zone" {
  type          = string
  default       = "asia-south1-a"
}
variable "environment" {
  type          = string
}
variable "app_name" {
  type          = string
}
variable "vm_count" {
  type          = number
}
variable "subnet_ip_cidr_range" {
  type          = string
}