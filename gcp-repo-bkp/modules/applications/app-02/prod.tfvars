region          = "asia-south1"
zone            = "asia-south1-b"
environment     = "prod"
app_name        = "supply-website-ns-org"
vm_count        = 4
subnet_ip_cidr_range    = "10.154.1.0/24"