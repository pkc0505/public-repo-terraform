region          = "asia-south1"
zone            = "asia-south1-b"
environment     = "stage"
app_name        = "supply-website-ns-org"
vm_count        = 2
subnet_ip_cidr_range    = "10.158.1.0/24"