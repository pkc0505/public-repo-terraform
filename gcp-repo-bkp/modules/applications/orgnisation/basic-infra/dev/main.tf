terraform {
  backend "gcs" {
      #configure backend
  }
}
provider "google" {}

module "vpc" {
    source      = "../../../../modules/network/vpc"
    environment = "dev"
}