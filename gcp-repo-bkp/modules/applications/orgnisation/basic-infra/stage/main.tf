terraform {
  backend "gcs" {
      #configure backend
  }
}
provider "google" {}

module "vpc" {
    source      = "../../../../modules/network/vpc"
    environment = "stage"
}

module "gcs" {
    source      = "../../../../modules/storage/gcs"
    environment = "stage"
    location    = "ASIA-SOUTH1"
}