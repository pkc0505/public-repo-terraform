#add provider
provider "google" {
    credentials = file("/Users/prashantkumar/.secure/test-netritvasadhana-org-2021-69778e3671a9.json")
    project     = "test-netritvasadhana-org-2021"
    region      = "asia-south1"
    zone        = "asia-south1-a"
}

resource "google_compute_network" "vpc-stage" {
  name                      = "vpc-stage"
  auto_create_subnetworks   = false
  description               = "This VPC is for stage enviornment"
}