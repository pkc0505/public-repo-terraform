module "gcs" {
    source      = "./../../core/storage/gcs"
    environment = "${var.environment}"
    location    = "${var.location}"
    name        = "${var.name}-${var.environment}"
}

variable "environment" {
  type          = string
}
variable "location" {
  type          = string
}
variable "name" {
  type          = string
}