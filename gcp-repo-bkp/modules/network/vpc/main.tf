module "vpc" {
    source      = "./../../core/network/vpc"
    environment = "${var.environment}"
}

variable "environment" {
  type          = string
}