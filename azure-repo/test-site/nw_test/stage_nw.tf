resource "azurerm_resource_group" "stg-network" {
  name     = "STG-NW-RG"
  location = "centralindia"
}

resource "azurerm_network_security_group" "stg-sec_grp-dmz3-01" {
  name                = "STG-NW-SECGRP"
  location            = azurerm_resource_group.stg-network.location
  resource_group_name = azurerm_resource_group.stg-network.name
}

resource "azurerm_virtual_network" "stg-vnet" {
  name                = "STG-VNET"
  location            = azurerm_resource_group.stg-network.location
  resource_group_name = azurerm_resource_group.stg-network.name
  address_space       = ["10.0.0.0/16"]

  subnet {
    name           = "STG-DMZ1"
    address_prefix = "10.0.1.0/24"
  }

  subnet {
    name           = "STG-DMZ2"
    address_prefix = "10.0.2.0/24"
  }

  subnet {
    name           = "STG-DMZ3"
    address_prefix = "10.0.3.0/24"
    security_group = azurerm_network_security_group.stg-sec_grp-dmz3-01.id
  }

  tags = {
    environment = "Stage",
    location = "centralindia"
  }
}

/** This is for DR **/

resource "azurerm_resource_group" "stg_dr-network" {
  name     = "STG_DR-NW-RG"
  location = "westindia"
}

resource "azurerm_network_security_group" "stg_dr-sec_grp-dmz3-01" {
  name                = "STG_DR-NW-SECGRP"
  location            = azurerm_resource_group.stg_dr-network.location
  resource_group_name = azurerm_resource_group.stg_dr-network.name
}

resource "azurerm_virtual_network" "stg_dr-vnet" {
  name                = "STG-VNET"
  location            = azurerm_resource_group.stg_dr-network.location
  resource_group_name = azurerm_resource_group.stg_dr-network.name
  address_space       = ["10.1.0.0/16"]

  subnet {
    name           = "STG_DR-DMZ1"
    address_prefix = "10.1.1.0/24"
  }

  subnet {
    name           = "STG_DR-DMZ2"
    address_prefix = "10.1.2.0/24"
  }

  subnet {
    name           = "STG_DR-DMZ3"
    address_prefix = "10.1.3.0/24"
    security_group = azurerm_network_security_group.stg_dr-sec_grp-dmz3-01.id
  }

  tags = {
    environment = "Stage",
    location = "westindia"
  }
}
