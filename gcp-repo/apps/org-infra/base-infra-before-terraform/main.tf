terraform {
  backend "gcs" {
      #configure backend
  }
}

provider "google" {}

module "gcs-bucket-for-terraform" {
    source      = "./../../../modules/services/gcs-bucket-for-terraform"
    #
    name          = "devops-terraform-states-4534534"
    location      = "ASIA-SOUTH1"
}
