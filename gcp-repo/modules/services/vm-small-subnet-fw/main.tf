module "tomcat-small-size" {
    source      = "./../../core-services/compute/vm-custom-size"
    #
    environment = var.environment
    app_name    = var.app_name
    # vm details
    vm_count        = var.vm_count
    vm_machine_type = "f1-micro"
    vm_image        = "debian-cloud/debian-9"
    # vm network details
    vm_network_id       = var.vm_network_id
    vm_subnetwork_name  = var.vm_subnetwork_name
}

variable "environment" { type = string }
variable "app_name" { type = string }

#virtual machine details
variable "vm_count" { 
    type = number
    default = 1
}
variable "vm_name" { type = string }
variable "vm_machine_type" { type = string }
variable "vm_zone" { type = string }
variable "vm_image" { type = string }

#virtual machine network details
variable "vm_network_id" { type = string }
variable "vm_subnetwork_name" { type = string }