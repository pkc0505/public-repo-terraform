module "gcs-bucket-for-terraform" {
    source      = "./../../core-services/storage/simple-object-storage"
    #
    name          = "${var.name}"
    location      = "${var.location}"
}

variable "name" { type = string }
variable "location" { 
    type = string 
    default = "ASIA-SOUTH1"
}