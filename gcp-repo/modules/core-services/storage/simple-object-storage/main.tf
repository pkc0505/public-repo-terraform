resource "google_storage_bucket" "gcs-bucket" {
    name          = "${var.name}"
    location      = "${var.location}"
    force_destroy = true
}

variable "location" {
  type      = string
}
variable "name" {
  type      = string
}