resource "random_id" "instance_id" {
    byte_length     = 8
}

data "google_compute_zones" "available" {}

resource "google_compute_instance" "custom-size" {
    count           = var.vm_count
    name            = var.vm_name
    machine_type    = var.vm_machine_type
    zone            = var.vm_zone

    tags = ["tf", "${var.environment}", "${var.app_name}", "vm-${var.app_name}-${var.environment}", 
    "vm-${var.app_name}-${count.index}-${var.environment}"]

    boot_disk {
        initialize_params {
            image = "${var.vm_image}"
        }
    }

    network_interface {
        #network     = "${data.google_compute_network.vpc.id}"
        network     = var.vm_network_id
        #subnetwork  = google_compute_subnetwork.subnet-app-specific.name
        subnetwork  = var.vm_subnetwork_name
    }

    metadata = {
        environment = "${var.environment}"
        app_name    = "${var.app_name}"
    }
}

variable "environment" { type = string }
variable "app_name" { type = string }

#virtual machine details
variable "vm_count" { type = number }
variable "vm_name" { type = string }
variable "vm_machine_type" { type = string }
variable "vm_zone" { type = string }
variable "vm_image" { type = string }

#virtual machine network details
variable "vm_network_id" { type = string }
variable "vm_subnetwork_name" { type = string }